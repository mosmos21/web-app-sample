package jp.co.unirita.webapp.sample.config;

import jp.co.unirita.webapp.sample.domain.account.Account;
import jp.co.unirita.webapp.sample.domain.account.AccountRepository;
import jp.co.unirita.webapp.sample.domain.master.category.CategoryMaster;
import jp.co.unirita.webapp.sample.domain.master.category.CategoryMasterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.Arrays;

public class InputData {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CategoryMasterRepository categoryMasterRepository;

    @PostConstruct
    public void insertUser() {
        accountRepository.saveAll(Arrays.asList(
                new Account("a", passwordEncoder.encode("a")),
                new Account("test1", passwordEncoder.encode("password")),
                new Account("test2", passwordEncoder.encode("password"))
        ));
    }

    @PostConstruct
    public void insertCategoryMaster() {
        categoryMasterRepository.saveAll(Arrays.asList(
                new CategoryMaster("食費"),
                new CategoryMaster("生活費"),
                new CategoryMaster("雑費")
        ));
    }
}
