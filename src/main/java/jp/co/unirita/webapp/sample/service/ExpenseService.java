package jp.co.unirita.webapp.sample.service;

import jp.co.unirita.webapp.sample.domain.expense.Expense;
import jp.co.unirita.webapp.sample.domain.expense.ExpenseRepository;
import jp.co.unirita.webapp.sample.form.expense.ExpenseForm;
import jp.co.unirita.webapp.sample.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;

@Service
public class ExpenseService {

    @Autowired
    ExpenseRepository expenseRepository;

    public void registerExpense(long accountId, ExpenseForm form) {
        Date date = DateUtil.createDate(form.getYear(), form.getMonth(), form.getDay());
        expenseRepository.save(new Expense(accountId, form.getCategory(), date, "", form.getPrice()));
    }
}
