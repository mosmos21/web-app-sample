package jp.co.unirita.webapp.sample.controller;

import jp.co.unirita.webapp.sample.constant.ModelKey;
import jp.co.unirita.webapp.sample.domain.account.Account;
import jp.co.unirita.webapp.sample.form.expense.ExpenseForm;
import jp.co.unirita.webapp.sample.service.CategoryService;
import jp.co.unirita.webapp.sample.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/expense")
public class ExpenseController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    ExpenseService expenseService;

    @GetMapping("/new")
    public String expenseForm(Model model) {
        model.addAttribute(ModelKey.CATEGORY_LIST, categoryService.getAllCategory());
        return "expense";
    }

    @PostMapping("/new")
    public String addExpense(ExpenseForm form, @AuthenticationPrincipal Account account) {
        expenseService.registerExpense(account.getId(), form);
        return "redirect:/";
    }
}
