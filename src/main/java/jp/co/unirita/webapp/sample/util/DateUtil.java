package jp.co.unirita.webapp.sample.util;

import java.sql.Date;
import java.util.Calendar;
import java.util.Optional;

public class DateUtil {

    private static Calendar cal = Calendar.getInstance();

    public static Date createDate(int year, int month) {
        return createDate(year, month, 1);
    }

    public static Date createDate(int year, int month, int day) {
        return new Date(year - 1900, month - 1, day);
    }

    public static int parseYear(Optional<String> optYear) {
        return Integer.parseInt(optYear.orElse(cal.get(Calendar.YEAR) + ""));
    }

    public static int parseMonth(Optional<String> optMonth) {
        return Integer.parseInt(optMonth.orElse((cal.get(Calendar.MONTH) + 1) + ""));
    }
}
