package jp.co.unirita.webapp.sample.controller;

import jp.co.unirita.webapp.sample.constant.ModelKey;
import jp.co.unirita.webapp.sample.domain.account.Account;
import jp.co.unirita.webapp.sample.domain.budget.Budget;
import jp.co.unirita.webapp.sample.domain.expense.Expense;
import jp.co.unirita.webapp.sample.form.budget.BudgetFormRow;
import jp.co.unirita.webapp.sample.service.BudgetService;
import jp.co.unirita.webapp.sample.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    BudgetService budgetService;

    @GetMapping
    public String index(
            Model model,
            @RequestParam(value = "year", required = false) Optional<String> optionalYear,
            @RequestParam(value = "month", required = false) Optional<String> optionalMonth,
            @AuthenticationPrincipal Account account) {

        int year = DateUtil.parseYear(optionalYear);
        int month = DateUtil.parseMonth(optionalMonth);
        List<BudgetFormRow> budgetList = budgetService.getBudgetFormRowList(account.getId(), year, month);
        List<Expense> expenseList = new ArrayList<>();

        log.info("[index()] accountId: {}, year: {}, month: {}, budgetList.size: {}",
                account.getId(), year, month, budgetList.size());
        model.addAttribute(ModelKey.YEAR, year);
        model.addAttribute(ModelKey.MONTH, month);
        model.addAttribute(ModelKey.BUDGET_LIST, budgetList);
        model.addAttribute(ModelKey.EXPENSE_LIST, expenseList);
        return "index";
    }
}
