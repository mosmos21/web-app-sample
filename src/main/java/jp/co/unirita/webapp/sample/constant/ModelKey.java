package jp.co.unirita.webapp.sample.constant;

public class ModelKey {
    public static final String YEAR = "year";
    public static final String MONTH = "month";

    public static final String CATEGORY_LIST = "categoryList";
    public static final String BUDGET_LIST = "budgetList";
    public static final String EXPENSE_LIST = "expenseList";
    public static final String TAGS = "tags";

    public static final String HAS_ERROR = "hasError";
    public static final String ERRORS = "errors";
}
