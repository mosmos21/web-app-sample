package jp.co.unirita.webapp.sample.form.expense;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExpenseFormRow {
    private long category;
    private int price;
    private int content;
}
